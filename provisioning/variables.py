# Define instance details
compartment_id = "ocid1.tenancy.oc1.<your-information>"
availability_domain = "rIdL:AP-SINGAPORE-1-AD-1"
shape = "VM.Standard.E2.1.Micro"
image_id = "ocid1.image.oc1.ap-singapore-1.<your-information>"
subnet_id = "ocid1.subnet.oc1.ap-singapore-1.<your-information>"
display_name = "ubuntu-amd"

# Path to the SSH public key file
ssh_public_key_file = "/Users/<your-information>/.ssh/id_rsa.pub"

# ntfy.sh key file
ntfy_key = "<your-information>"