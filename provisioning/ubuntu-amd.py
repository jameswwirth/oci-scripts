import oci
import logging
from variables import compartment_id, availability_domain, shape, image_id, subnet_id, ssh_public_key_file, display_name

# Enable debug logging for OCI SDK
oci.config.logger.setLevel(logging.DEBUG)

# Load OCI configuration
config = oci.config.from_file()

# Initialize the ComputeClient
compute_client = oci.core.ComputeClient(config)

# Read SSH key from file
with open(ssh_public_key_file, "r") as f:
    ssh_key = f.read().strip()

# Define SSH key in metadata
metadata = {"ssh_authorized_keys": ssh_key}

# Create instance details
create_instance_details = oci.core.models.LaunchInstanceDetails(
    compartment_id=compartment_id,
    availability_domain=availability_domain,
    shape=shape,
    display_name=display_name,
    image_id=image_id,
    subnet_id=subnet_id,
    metadata=metadata
)

try:
    # Provision the instance
    response = compute_client.launch_instance(create_instance_details)

    # Wait for the instance to be in 'RUNNING' state
    instance_id = response.data.id
    get_instance_response = compute_client.get_instance(instance_id)
    instance = get_instance_response.data
    while instance.lifecycle_state != 'RUNNING':
        get_instance_response = compute_client.get_instance(instance_id)
        instance = get_instance_response.data

    print("Instance provisioned successfully!")
    print("Instance OCID:", instance.id)
    print("Instance Public IP:", instance.public_ip)

except oci.exceptions.ServiceError as e:
    print("An error occurred:", e)
