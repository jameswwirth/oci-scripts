# oci-scripts

## Description
This is a collection or potentially only a single script for use with OCI.


## Usage
You need python and all the variables. A way to get most of the variables is to create a resource, save it as a stack then review the information in the stack.

The script could be regularly scheduled using a cron job if you wanted to continually request resources on a set schedule.

## Contributing
Contributions are welcome

## License
For open source projects, say how it is licensed.